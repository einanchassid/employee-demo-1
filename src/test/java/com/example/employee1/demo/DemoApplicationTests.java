package com.example.employee1.demo;

import com.example.employee1.demo.controllers.*;
import com.example.employee1.demo.entity.*;
import com.example.employee1.demo.services.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;

@SpringBootTest
class DemoApplicationTests {
    @Autowired
    EmployeeService employeeService;
    @Autowired
    GeneralDetailsService generalDetailsService;
    @Autowired
    SpouseService spouseService;
    @Autowired
    ChildService childService;
    @Autowired
    AddressService addressService;

    @Bean
    @Test
    void contextLoads() {
        Employee employee = new Employee();
        employee.setFirstname("israel");
        employee.setLastname("israeli");
        employeeService.createEmployee(employee);
        employee.setFirstname("moshe");
        employeeService.updateEmployee(employee.getId(),employee);
        employeeService.getEmployeeById(employee.getId());

        GeneralDetails generalDetails = new GeneralDetails();
        generalDetails.setEmployee(employee);
        generalDetails.setPhone("0523344555");
        generalDetails.setEmail("israel@gmail.com");
        generalDetailsService.createGeneralDetails(generalDetails);
        generalDetails.setPhone("097821334");
        generalDetailsService.updateGeneralDetails(employee.getId(),generalDetails);
        generalDetailsService.getGeneralDetailsById(generalDetails.getId());

        Spouse spouse = new Spouse();
        spouse.setEmployee(employee);
        spouse.setFirstname("israela");
        spouse.setLastname("israeli");
        spouseService.createSpouse(spouse);
        spouse.setFirstname("anna");
        spouseService.updateSpouse(employee.getId(),spouse);
        spouseService.getSpouseById(spouse.getId());

        Child child = new Child();
        child.setFirstname("devid");
        child.setLastname("chassid");
        child.setEmployee(employee);
        childService.createChild(child);
        child.setLastname("israeli");
        childService.updateChild(employee.getId(),child);
        childService.getChildById(child.getId());

        Address address = new Address();
        address.setEmployee(employee);
        address.setFulladdress("dizingof 200 Tel-aviv");
        address.setPhone("037143333");
        addressService.createAddress(address);
        address.setPhone("097991345");
        addressService.updateAddress(employee.getId(),address);
        addressService.getAddressById(address.getId());

    }

}
