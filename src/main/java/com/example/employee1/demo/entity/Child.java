package com.example.employee1.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
public class Child implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    private String firstname;
    private String lastname;
    @JsonIgnore
    @ManyToOne
    private Employee employee;

    public Child() {
    }

    public Child(String id, String firstname, String lastname ,String empid) {
        super();
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.employee = new Employee(empid, " "," ");

    }

    // getters and setters, equals(), toString() .... (omitted for brevity)
}