package com.example.employee1.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class GeneralDetails implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    private String phone;
    private String email;
    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    private Employee employee;


    public GeneralDetails(String id, String phone, String email , String empid) {
        super();
        this.id = id;
        this.phone = phone;
        this.email = email;
        this.employee = new Employee(empid, " "," ");

    }

    // getters and setters, equals(), toString() .... (omitted for brevity)
}