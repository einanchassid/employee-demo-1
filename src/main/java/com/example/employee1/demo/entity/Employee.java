package com.example.employee1.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Data
public class Employee implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    private String firstname;
    private String lastname;
    @OneToOne(mappedBy = "employee", fetch = FetchType.LAZY)
    private GeneralDetails generalDetails;
    @OneToOne(mappedBy = "employee", fetch = FetchType.LAZY)
    private Spouse spouse;
    @OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
    private List<Address> addressList;
    @OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
    private List<Child> childList;


    public Employee(String id,String firstname, String lastname) {
        super();
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;

    }

    // getters and setters, equals(), toString() .... (omitted for brevity)
}