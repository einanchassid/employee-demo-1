package com.example.employee1.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
public class Address implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    private String fulladdress;
    private String phone;
    @ManyToOne
    @JsonIgnore
    private Employee employee;

    public Address() {
    }

    public Address(String id, String fulladdress, String phone , String empid) {
        super();
        this.id = id;
        this.fulladdress = fulladdress;
        this.phone = phone;
        this.employee = new Employee(empid, " "," ");

    }

    // getters and setters, equals(), toString() .... (omitted for brevity)
}