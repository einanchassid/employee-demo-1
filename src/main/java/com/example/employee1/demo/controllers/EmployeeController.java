package com.example.employee1.demo.controllers;

import com.example.employee1.demo.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.example.employee1.demo.services.EmployeeService;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/")
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;
    private String id;
    private Employee employeeRequest;

    @GetMapping("/employee")
    public List<Employee> getEmployees() {

        return employeeService.getAllEmployees();
    }

    @GetMapping("/employee/{id}")
    public Optional<Employee> getEmployee(@PathVariable String id) {
        Optional<Employee> employee = null;
        employee = employeeService.getEmployeeById(id);
        return employee;
    }
    @PostMapping("/employee")
    public Employee postEmployee(@RequestBody Employee employee) {
        return employeeService.createEmployee(employee);
    }

    @PutMapping("/employee/{id}")
    public Employee putEmployee(@PathVariable String id ,@RequestBody Employee employeeRequest) {
        return employeeService.updateEmployee(id,employeeRequest);
    }


    @DeleteMapping("/employee/{id}")
    public ResponseEntity<?> deleteEmployee(@PathVariable String  id) {
        return employeeService.deleteEmployee(id);
    }
}

