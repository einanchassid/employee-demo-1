package com.example.employee1.demo.controllers;

import com.example.employee1.demo.entity.Employee;
import com.example.employee1.demo.entity.GeneralDetails;
import com.example.employee1.demo.entity.Spouse;
import com.example.employee1.demo.services.GeneralDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/employee")
public class GeneralDetailsController {
    @Autowired
    private GeneralDetailsService generaldetailsService;
    private String id;
    private GeneralDetails generaldetailsRequest;

    @GetMapping("/{empid}/generaldetails")
    public List<GeneralDetails> getGeneralDetailss(@PathVariable String empid) {

        return generaldetailsService.getAllGeneralDetailss(empid);
    }

    @GetMapping("/{empid}/generaldetails/{id}")
    public Optional<GeneralDetails> getGeneralDetails(@PathVariable String id) {
        Optional<GeneralDetails> generaldetails = null;
        generaldetails = generaldetailsService.getGeneralDetailsById(id);
        return generaldetails;
    }
    @PostMapping("/{empid}/generaldetails")
    public GeneralDetails postGeneralDetails(@RequestBody GeneralDetails generaldetails, @PathVariable String empid) {
        generaldetails.setEmployee(new Employee(empid,"",""));
        return generaldetailsService.createGeneralDetails(generaldetails);
    }

    @PutMapping("/{empid}/generaldetails/{id}")
    public GeneralDetails putGeneralDetails(@PathVariable String id, @RequestBody GeneralDetails generaldetailsRequest, @PathVariable String empid) {
        generaldetailsRequest.setEmployee(new Employee(empid,"",""));
        return generaldetailsService.updateGeneralDetails(id,generaldetailsRequest);
    }


    @DeleteMapping("/{empid}/generaldetails/{id}")
    public ResponseEntity<?> deleteGeneralDetails(@PathVariable String id, @PathVariable String empid) {
        return generaldetailsService.deleteGeneralDetails(id);
    }
}

