package com.example.employee1.demo.controllers;

import com.example.employee1.demo.entity.Spouse;
import com.example.employee1.demo.entity.Employee;
import com.example.employee1.demo.services.SpouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/employee")
public class SpouseController {
    @Autowired
    private SpouseService spouseService;
    private String id;
    private Spouse spouseRequest;

    @GetMapping("/{empid}/spouse")
    public List<Spouse> getSpouses(@PathVariable String empid) {

        return spouseService.getAllSpouses(empid);
    }

    @GetMapping("/{empid}/spouse/{id}")
    public Optional<Spouse> getSpouse(@PathVariable String id) {
        Optional<Spouse> spouse = null;
        spouse = spouseService.getSpouseById(id);
        return spouse;
    }
    @PostMapping("/{empid}/spouse")
    public Spouse postSpouse(@RequestBody Spouse spouse, @PathVariable String empid) {
        spouse.setEmployee(new Employee(empid,"",""));
        return spouseService.createSpouse(spouse);
    }

    @PutMapping("/{empid}/spouse/{id}")
    public Spouse putSpouse(@PathVariable String id, @RequestBody Spouse spouseRequest, @PathVariable String empid) {
        spouseRequest.setEmployee(new Employee(empid,"",""));
        return spouseService.updateSpouse(id,spouseRequest);
    }


    @DeleteMapping("/{empid}/spouse/{id}")
    public ResponseEntity<?> deleteSpouse(@PathVariable String id, @PathVariable String empid) {
        return spouseService.deleteSpouse(id);
    }
}

