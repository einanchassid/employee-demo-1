package com.example.employee1.demo.controllers;

import com.example.employee1.demo.entity.Address;
import com.example.employee1.demo.entity.Employee;
import com.example.employee1.demo.services.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/employee")
public class AddressController {
    @Autowired
    private AddressService addressService;
    private String id;
    private Address addressRequest;

    @GetMapping("/{empid}/address")
    public List<Address> getAddresses(@PathVariable String empid) {
        return addressService.getAllAddresses(empid);
    }

    @GetMapping("/{empid}/address/{id}")
    public Optional<Address> getAddress(@PathVariable String id) {
        Optional<Address> address = null;
        address = addressService.getAddressById(id);
        return address;
    }
    @PostMapping("/{empid}/Address")
    public Address postAddress(@RequestBody Address address, @PathVariable String empid) {
        address.setEmployee(new Employee(empid,"",""));
        return addressService.createAddress(address);
    }

    @PutMapping("/{empid}/address/{id}")
    public Address putAddress(@PathVariable String id, @RequestBody Address addressRequest, @PathVariable String empid) {
        addressRequest.setEmployee(new Employee(empid,"",""));
        return addressService.updateAddress(id,addressRequest);
    }


    @DeleteMapping("/{empid}/address/{id}")
    public ResponseEntity<?> deleteAddress(@PathVariable String id, @PathVariable String empid) {
        return addressService.deleteAddress(id);
    }
}

