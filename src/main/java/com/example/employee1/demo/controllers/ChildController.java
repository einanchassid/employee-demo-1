package com.example.employee1.demo.controllers;

import com.example.employee1.demo.entity.Child;
import com.example.employee1.demo.entity.Employee;
import com.example.employee1.demo.services.ChildService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/employee")
public class ChildController {
    @Autowired
    private ChildService childService;
    private String id;
    private Child childRequest;

    @GetMapping("/{empid}/child")
    public List<Child> getChilds(@PathVariable String empid) {

        return childService.getAllChilds(empid);
    }

    @GetMapping("/{empid}/child/{id}")
    public Optional<Child> getChild(@PathVariable String id) {
        Optional<Child> child = null;
        child = childService.getChildById(id);
        return child;
    }
    @PostMapping("/{empid}/child")
    public Child postChild(@RequestBody Child child, @PathVariable String empid) {
        child.setEmployee(new Employee(empid,"",""));
        return childService.createChild(child);
    }

    @PutMapping("/{empid}/child/{id}")
    public Child putChild(@PathVariable String id, @RequestBody Child childRequest, @PathVariable String empid) {
        childRequest.setEmployee(new Employee(empid,"",""));
        return childService.updateChild(id,childRequest);
    }


    @DeleteMapping("/{empid}/child/{id}")
    public ResponseEntity<?> deleteChild(@PathVariable String id, @PathVariable String empid) {
        return childService.deleteChild(id);
    }
}

