package com.example.employee1.demo.services;

import com.example.employee1.demo.entity.GeneralDetails;
import com.example.employee1.demo.repositories.GeneralDetailsRepository;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GeneralDetailsService {
    @Autowired
    private GeneralDetailsRepository generalDetailsRepository;

    public List<GeneralDetails> getAllGeneralDetailss(String id) {
        return generalDetailsRepository.findAllByEmployeeId(id);
    }

    public Optional<GeneralDetails> getGeneralDetailsById(String id) {
        Optional<GeneralDetails> generaldetails = generalDetailsRepository.findById(id);
        return generaldetails;
    }

    public GeneralDetails createGeneralDetails(GeneralDetails generaldetails) {

        return generalDetailsRepository.save(generaldetails);
    }

    public GeneralDetails updateGeneralDetails(String id,GeneralDetails generaldetailsRequest) {
        return generalDetailsRepository.findById(id).map(generaldetails -> {
            generaldetails.setPhone(generaldetailsRequest.getPhone());
            generaldetails.setEmail(generaldetailsRequest.getEmail());
            return generalDetailsRepository.save(generaldetails);
        }).orElseThrow(() -> new ResourceNotFoundException("id " + id + " not found"));
    }

    public ResponseEntity<?> deleteGeneralDetails(String id) {
        return generalDetailsRepository.findById(id).map(generaldetails -> {
            generalDetailsRepository.delete(generaldetails);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("id " + id + " not found"));
    }


}
