package com.example.employee1.demo.services;

import com.example.employee1.demo.entity.Child;
import com.example.employee1.demo.entity.Employee;
import com.example.employee1.demo.repositories.ChildRepository;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ChildService {
    @Autowired
    private ChildRepository childRepository;

    public List<Child> getAllChilds(String id) {
        return childRepository.findAllByEmployeeId(id);
    }

    public Optional<Child> getChildById(String id) {
        Optional<Child> child = childRepository.findById(id);
        return child;
    }

    public Child createChild(Child child) {

        return childRepository.save(child);
    }

    public Child updateChild(String id,Child childRequest) {
        return childRepository.findById(id).map(child -> {
            child.setFirstname(childRequest.getFirstname());
            child.setLastname(childRequest.getLastname());
            return childRepository.save(child);
        }).orElseThrow(() -> new ResourceNotFoundException("id " + id + " not found"));
    }

    public ResponseEntity<?> deleteChild(String id) {
        return childRepository.findById(id).map(child -> {
            childRepository.delete(child);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("id " + id + " not found"));
    }


}
