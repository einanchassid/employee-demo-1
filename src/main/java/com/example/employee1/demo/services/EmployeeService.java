package com.example.employee1.demo.services;

import com.example.employee1.demo.entity.Employee;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.example.employee1.demo.repositories.EmployeeRepository;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    public Optional<Employee> getEmployeeById(String id) {
        Optional<Employee> employee = employeeRepository.findById(id);
        return employee;
    }

    public Employee createEmployee(Employee employee) {

        return employeeRepository.save(employee);
    }

    public Employee updateEmployee(String id,Employee employeeRequest) {
        return employeeRepository.findById(id).map(employee -> {
            employee.setFirstname(employeeRequest.getFirstname());
            employee.setLastname(employeeRequest.getLastname());
            return employeeRepository.save(employee);
        }).orElseThrow(() -> new ResourceNotFoundException("id " + id + " not found"));
    }

    public ResponseEntity<?> deleteEmployee(String id) {
        return employeeRepository.findById(id).map(employee -> {
            employeeRepository.delete(employee);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("id " + id + " not found"));
    }


}
