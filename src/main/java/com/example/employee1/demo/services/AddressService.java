package com.example.employee1.demo.services;

import com.example.employee1.demo.entity.Address;
import com.example.employee1.demo.repositories.AddressRepository;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AddressService {
    @Autowired
    private AddressRepository addressRepository;

    public List<Address> getAllAddresses(String id) {
        return addressRepository.findAllByEmployeeId(id);
    }

    public Optional<Address> getAddressById(String id) {
        Optional<Address> address = addressRepository.findById(id);
        return address;
    }

    public Address createAddress(Address address) {

        return addressRepository.save(address);
    }

    public Address updateAddress(String id,Address addressRequest) {
        return addressRepository.findById(id).map(address -> {
            address.setFulladdress(addressRequest.getFulladdress());
            address.setPhone(addressRequest.getPhone());
            return addressRepository.save(address);
        }).orElseThrow(() -> new ResourceNotFoundException("id " + id + " not found"));
    }

    public ResponseEntity<?> deleteAddress(String id) {
        return addressRepository.findById(id).map(address -> {
            addressRepository.delete(address);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("id " + id + " not found"));
    }


}
