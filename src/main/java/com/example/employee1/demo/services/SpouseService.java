package com.example.employee1.demo.services;

import com.example.employee1.demo.entity.Spouse;
import com.example.employee1.demo.repositories.SpouseRepository;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SpouseService {
    @Autowired
    private SpouseRepository spouseRepository;

    public List<Spouse> getAllSpouses(String id) {
        return spouseRepository.findAllByEmployeeId(id);
    }

    public Optional<Spouse> getSpouseById(String id) {
        Optional<Spouse> spouse = spouseRepository.findById(id);
        return spouse;
    }

    public Spouse createSpouse(Spouse spouse) {

        return spouseRepository.save(spouse);
    }

    public Spouse updateSpouse(String id,Spouse spouseRequest) {
        return spouseRepository.findById(id).map(spouse -> {
            spouse.setFirstname(spouseRequest.getFirstname());
            spouse.setLastname(spouseRequest.getLastname());
            return spouseRepository.save(spouse);
        }).orElseThrow(() -> new ResourceNotFoundException("id " + id + " not found"));
    }

    public ResponseEntity<?> deleteSpouse(String id) {
        return spouseRepository.findById(id).map(spouse -> {
            spouseRepository.delete(spouse);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("id " + id + " not found"));
    }


}
