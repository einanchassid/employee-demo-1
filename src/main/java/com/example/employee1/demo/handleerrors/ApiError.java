package com.example.employee1.demo.handleerrors;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
@Data
class ApiError {

    private HttpStatus errorCode;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDateTime timestamp;
    private String userMesseg;
    private String aplicationName = "employeeDemo";

    private ApiError() {
        timestamp = LocalDateTime.now();
    }

    ApiError(HttpStatus errorCode) {
        this();
        this.errorCode = errorCode;
    }

    ApiError(HttpStatus errorCode, Throwable ex) {
        this();
        this.errorCode = errorCode;
        this.userMesseg = "Unexpected error";
    }

    ApiError(HttpStatus errorCode, String userMesseg, Throwable ex) {
        this();
        this.errorCode = errorCode;
        this.userMesseg = userMesseg;
    }
}
