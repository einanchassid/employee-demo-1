package com.example.employee1.demo.repositories;

import com.example.employee1.demo.entity.Child;
import com.example.employee1.demo.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChildRepository extends JpaRepository<Child, String> {

    public List<Child> findAllByEmployeeId(String id);

}