package com.example.employee1.demo.repositories;

import com.example.employee1.demo.entity.Address;
import com.example.employee1.demo.entity.Child;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AddressRepository extends JpaRepository<Address, String> {

    public List<Address> findAllByEmployeeId(String id);

}