package com.example.employee1.demo.repositories;

import com.example.employee1.demo.entity.GeneralDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GeneralDetailsRepository extends JpaRepository<GeneralDetails, String> {

    List<GeneralDetails> findAllByEmployeeId(String id);

}