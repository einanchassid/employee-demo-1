package com.example.employee1.demo.repositories;

import com.example.employee1.demo.entity.Spouse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SpouseRepository extends JpaRepository<Spouse, String> {

    public List<Spouse> findAllByEmployeeId(String id);

}